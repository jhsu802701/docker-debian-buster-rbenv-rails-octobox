# Docker Debian Buster - rbenv - Rails - Octobox

This repository is used for building a custom Docker image for [Octobox](https://github.com/octobox/octobox).

## Name of This Docker Image
[registry.gitlab.com/jhsu802701/docker-debian-buster-rbenv-rails-octobox](https://gitlab.com/jhsu802701/docker-debian-buster-rbenv-rails-octobox/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-buster-min-rbenv](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-rbenv/container_registry)

## What's Added
* The latest versions of the rails, pg, nokogiri, and ffi gems
* The versions of the above gems used in the Octobox app
* The mailcatcher gem
* The correct Ruby version WITH the above gems plus the Ruby version to upgrade to
* MySQL

## More Information
General information common to all Docker Debian Buster build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-debian-buster-common/blob/master/FAQ.md).
